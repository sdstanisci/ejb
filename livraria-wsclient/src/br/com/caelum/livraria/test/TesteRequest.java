package br.com.caelum.livraria.test;

import java.rmi.RemoteException;
import br.com.caelum.livraria.ws.LivrariaWS;
import br.com.caelum.livraria.ws.LivrariaWSProxy;
import br.com.caelum.livraria.ws.Livro;

public class TesteRequest {

	public static void main(String[] args) throws RemoteException {
		
		LivrariaWS cliente = new LivrariaWSProxy();
		Livro[] livros = cliente.getLivrosPeloNome("a");
		
		for (Livro livro : livros) {
			System.out.println("Autor: " + livro.getAutor().getNome());
			System.out.println("Autor: " + livro.getTitulo());
		}
		
		
	}
	
}
