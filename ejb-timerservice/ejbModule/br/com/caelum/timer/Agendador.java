package br.com.caelum.timer;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton // usamos este ao inves de stateless por nao precisar de varias instancias de um agendamento
@Startup // carrega no inicio da aplicacao
public class Agendador {
	// o * defini que pode ser chamado a qualquer momento, mas poderia definir por hr por ex. com o hour="9,18"
	// o */ significa que sera executado a cada 10 Segundos
	@Schedule(hour="*", minute="*", second="*/10", persistent=false)
	void agendado(){
		System.out.println("agendamento realizado com sucesso !");
	}
	
}
