package br.com.caelum.livraria.interceptador;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LogInterceptador {

	// anotacao para o container fazer a interceptacao e poder medir os tempos de execucao
	@AroundInvoke
	public Object intercepta(InvocationContext context) throws Exception{
		
		Long tempoInicial = System.currentTimeMillis();
		
		Object object = context.proceed();
		
		String nomeClasse = context.getTarget().getClass().getSimpleName();
		String nomeMetodo = context.getMethod().getName();
		
		Long tempoFinal = System.currentTimeMillis();
		
		System.out.println("Classe: " + nomeClasse);
		System.out.println("Metodo: " + nomeMetodo);
		System.out.println("Tempo: " + (tempoFinal - tempoInicial));
		
		return object;
	}

}
