package br.com.caelum.livraria.dao;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.caelum.livraria.modelo.Usuario;

@Stateless
public class UsuarioDao {

	@PersistenceContext(unitName="unidade_persistencia_ejb")
    private EntityManager manager;
	
	@PostConstruct
	void aposCriar(){
		System.out.println("AutorDao foi criado");
	}
	
	public Usuario buscaPeloLogin(Usuario usuario) {

		Usuario usuarioEncontrado = null;
		try {

			TypedQuery<Usuario> query = manager
					.createQuery("select u from Usuario u where u.login =:pLogin and u.senha = :pSenha", Usuario.class);

			query.setParameter("pLogin", usuario.getLogin());
			query.setParameter("pSenha", usuario.getSenha());

			usuarioEncontrado = query.getSingleResult();

		} catch (NoResultException e) {
			//em.close();// a injecao de dependencia do LoginBean cuida de fechar o em
			return null;
		}
		// em.close();// a injecao de dependencia do LoginBean cuida de fechar o em
		// em.close();
		return usuarioEncontrado;
	}

}
