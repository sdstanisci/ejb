package br.com.caelum.livraria.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import br.com.caelum.livraria.modelo.Livro;

@Stateless
public class LivroDao {

	@PersistenceContext(unitName="unidade_persistencia_ejb")
    private EntityManager manager;
	
	public void salva(Livro livro) {
		manager.persist(livro);
	}
	
	public List<Livro> todosLivros() {
		return manager.createQuery("select l from Livro l", Livro.class).getResultList();
	}

	public List<Livro> livrosPeloNome(String nome) {
		
		TypedQuery<Livro> query = manager.createQuery("select l from Livro l where l.titulo like :pNomeLivro", Livro.class);
		query.setParameter("pNomeLivro", '%' + nome + '%');
		List<Livro> livros = query.getResultList();
		
		return livros;
	}
	
}
